package id.dwikiriyadi.note.ui.section

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import id.dwikiriyadi.note.data.repository.SectionRepository

class SectionViewModelFactory(private val repository: SectionRepository): ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>) = SectionViewModel(repository) as T
}