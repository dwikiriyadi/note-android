package id.dwikiriyadi.note.data.repository

import id.dwikiriyadi.note.data.dao.PageDao
import id.dwikiriyadi.note.data.model.Page

class PageRepository private constructor(private val pageDao: PageDao){

    fun all(id: Long) = pageDao.all(id)

    fun get(id: Long) = pageDao.get(id)

    fun insert(page: Page) = pageDao.insert(page)

    fun delete(page: Page) = pageDao.delete(page)

    fun update(page: Page) = pageDao.update(page)

    companion object {
        @Volatile private var instance: PageRepository? = null

        fun getInstance(pageDao: PageDao) = instance ?: synchronized(this) {
            instance ?: PageRepository(pageDao).also { instance = it }
        }
    }
}